import React, { Fragment } from 'react';
import { createRoot } from 'react-dom/client';


import MenuItem from './component/MenuItem';
import Menu from './component/Menu';

 
const root = createRoot(document.getElementById('root'));

let menuItemWording = ['Like的發問', 'Like的回答', 'Like的文章', 'Like的留言'];
let menuItemArr = menuItemWording.map(wording => <MenuItem text={wording}/>);


const handleClick = event => {
    console.log(event.target.value);
};

root.render(
    <>
        <Menu title={'Ivan Cheng的Like'}>{menuItemArr}</Menu>

        <div>hello React!</div>
        {true && <button value={87} onClick={handleClick}>Click Me</button>}

        <div>{'10+50='+(10+50)}</div>
    </>
);
